#!/bin/bash

############################################################
# This script provides a better diff when using terragrunt.
# Usage: [ENV values] terragruntdiff.sh [terragrunt options]

# Limitations:
# This can only be done on a single module, and not for terragrunt run-all.

# ENV values:
# - SHOW_RAW=1: echo the terragrunt raw plan
# - SHOW_CHANGES=1: echo the terragrunt raw changes
# - CONVERT_NEWLINES=1: replace \n with actual newline in diff
############################################################

set -euo pipefail

# Check for dependencies
if ! command -v terraform &> /dev/null; then
  echo "Error: 'terraform' is not installed. Please install it to proceed." >&2
  exit 1
elif ! command -v terragrunt &> /dev/null; then
  echo "Error: 'terragrunt' is not installed. Please install it to proceed." >&2
  exit 1
elif ! command -v jq &> /dev/null; then
  echo "Error: 'jq' is not installed. Please install it to proceed." >&2
  exit 1
elif ! command -v meld &> /dev/null; then
  echo "Error: 'meld' is not installed. Please install it to proceed." >&2
  exit 1
fi

# Create temp file and cleanup when script exits
TMPFILE=$(mktemp /tmp/terragrunt-diff.XXXXXX)
trap "rm -f ${TMPFILE}" EXIT INT TERM

# Get plan and save in temp file
terragrunt plan -out="${TMPFILE}" $@ > /dev/null

# Convert plan to json format
PLAN_RAW=$(terragrunt show -json ${TMPFILE})

# Show raw plan if desired
if [ ! -z ${SHOW_RAW+x} ]; then
  echo "${PLAN_RAW}"
  exit 0
fi

# Get resource_changes (ignore resource_drift)
CHANGES=$(echo "${PLAN_RAW}" | jq '.resource_changes[]')

if [ ! -z ${SHOW_CHANGES+x} ]; then
  echo "${CHANGES}"
  exit 0
fi

if [ ! -z ${CONVERT_NEWLINES+x} ]; then
  NEWLINE_CONVERTOR="sed 's:\\\\n:\n:g'"
else
  # Passthrough (do nothing)
  NEWLINE_CONVERTOR="cat"
fi

# diff
# - conditions
#   - ignore sections that have change.action = "no-op" or "read"
# - output
#   - show the address (module name)
#   - show the change action (understand what change action is happening)
#   - show the change diff (before and after)
# diff --color=always -u \
meld \
  <(echo "$CHANGES" | jq '. | select(.change.actions[] | contains("no-op") or contains("read") | not) | { "address": .address, "action": .change.actions, "change": .change.before }' | eval "$NEWLINE_CONVERTOR") \
  <(echo "$CHANGES" | jq '. | select(.change.actions[] | contains("no-op") or contains("read") | not) | { "address": .address, "action": .change.actions, "change": .change.after }' | eval "$NEWLINE_CONVERTOR")
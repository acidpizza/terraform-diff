#!/bin/bash

############################################################
# This script provides a better diff when using terraform.
# Usage: [ENV values] terraform.sh [terraform options]

# ENV values:
# - SHOW_RAW=1: echo the terraform raw plan
# - SHOW_CHANGES=1: echo the terraform raw changes
# - CONVERT_NEWLINES=1: replace \n with actual newline in diff
############################################################

set -euo pipefail

# Check for dependencies
if ! command -v terraform &> /dev/null; then
  echo "Error: 'terraform' is not installed. Please install it to proceed." >&2
  exit 1
elif ! command -v jq &> /dev/null; then
  echo "Error: 'jq' is not installed. Please install it to proceed." >&2
  exit 1
elif ! command -v meld &> /dev/null; then
  echo "Error: 'meld' is not installed. Please install it to proceed." >&2
  exit 1
fi

# Create temp file and cleanup when script exits
TMPFILE=$(mktemp /tmp/terraform-diff.XXXXXX)
trap "rm -f ${TMPFILE}" EXIT INT TERM

# Get plan and save in temp file
terraform plan -out="${TMPFILE}" $@ > /dev/null

# Convert plan to json format
PLAN_RAW=$(terraform show -json ${TMPFILE})

# Show raw plan if desired
if [ ! -z ${SHOW_RAW+x} ]; then
  echo "${PLAN_RAW}"
  exit 0
fi

# Get resource_changes (ignore resource_drift)
CHANGES=$(echo "${PLAN_RAW}" | jq '.resource_changes[].change')

if [ ! -z ${SHOW_CHANGES+x} ]; then
  echo "${CHANGES}"
  exit 0
fi

if [ ! -z ${CONVERT_NEWLINES+x} ]; then
  NEWLINE_CONVERTOR="sed 's:\\\\n:\n:g'"
else
  # Passthrough (do nothing)
  NEWLINE_CONVERTOR="cat"
fi

# diff
# diff --color=always -u \
meld \
  <(echo "$CHANGES" | jq '.before' | eval "$NEWLINE_CONVERTOR") \
  <(echo "$CHANGES" | jq '.after' | eval "$NEWLINE_CONVERTOR")

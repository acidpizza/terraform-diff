# Terraform Diff

This project provides helper scripts to make terraform and terragrunt plan outputs easier to digest.

## Pre-Requisities

Need `meld` and `jq`.

## Setup

Simply add the 2 scripts to /usr/local/bin to use.

```shell
ln -s $PWD/terraformdiff.sh /usr/local/bin/terraformdiff.sh
ln -s $PWD/terragruntdiff.sh /usr/local/bin/terragruntdiff.sh
```

## Usage

Use env values to show raw outputs to aid understanding on the diffs.

### Terraformdiff

```shell
# [ENV values] terraformdiff.sh [terraform options]
# - SHOW_RAW=1: echo the terraform raw plan
# - SHOW_CHANGES=1: echo the terraform raw changes
# - CONVERT_NEWLINES=1: replace \n with actual newline in diff
SHOW_CHANGES=1 terraformdiff.sh -var-file variables.tfvars
```

### Terragruntdiff

```shell
# [ENV values] terragruntdiff.sh [terragrunt options]
# - SHOW_RAW=1: echo the terragrunt raw plan
# - SHOW_CHANGES=1: echo the terragrunt raw changes
# - CONVERT_NEWLINES=1: replace \n with actual newline in diff
CONVERT_NEWLINES=1 terragruntdiff.sh
```